# jobaid-connector project

If you new to VueJs, see the steps to create sample vue project - https://www.tutorialspoint.com/vuejs/vuejs_environment_setup.htm

# SSH Key generation 

step1. ssh-keygen -t ed25519 -C "<comment>"

step2. Paste the following in the ssh key 
cat ~/.ssh/id_ed25519.pub | clip

step3. test the connection
git clone git@gitlab.com:gitlab-tests/sample-project.git

=======================================================================
install gradle - https://gradle.org/install/

node version 
node --version
v10.14.1

npm version
npm --version
6.4.1

=======================================================================

git clone git@gitlab.com:debojyoti.bose.db/jobaid-connector.git

=======================================================================

npm install vue
npm install vue-cli
npm install vue-router
npm install axios
npm i @vue/cli-service
npm i vue-template-compiler

To run the frontend application - 
cd ~/workspace/jobaid-connector/jobaid-frontend
npm run serve

=======================================================================
Install MySql and create a schema jobaid

spring.datasource.url=jdbc:mysql://localhost:3306/jobaid
spring.datasource.username=root
spring.datasource.password=system

execute the following query - 

CREATE TABLE `customer` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `first_name` VARCHAR(100) NULL,
  `last_name` VARCHAR(100) NULL,
  `email` VARCHAR(100) NULL,
  `phone` VARCHAR(20) NULL,
  PRIMARY KEY (`id`));
  
=======================================================================
