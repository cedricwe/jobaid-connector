import http from '../http-common'

class CustomerDataService {
    getAll() {
        return http.get('/jobaid-customers')
    }

    get(id) {
        return http.get(`/jobaid-customers/${id}`)
    }

    create(data) {
        return http.post('/jobaid-customers', data)
    }

    update(id, data) {
        return http.put(`/jobaid-customers/${id}`, data)
    }

    delete(id) {
        return http.delete(`/jobaid-customers/${id}`)
    }
}

export default new CustomerDataService()
